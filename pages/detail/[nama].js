
// useRequest.js
 
// import useSWR from 'swr'
import config from '../../config.json'
import { useRouter } from 'next/router'
import InputSukses from '../../components/Inputsukses'

const Post = (props) => {
  const router = useRouter()
  const { nama } = router.query
  const { pokemon } = props
  const { sprites, name } = pokemon
  return (
    <main className='App'>
      <h1>Detail Pokemon</h1>
      <div className="text-left text-center col-2">
        <a href="/">
          <button className="btn btn-secondary mr-2">Back</button>
        </a>
      </div>
      <div className="row justify-content-center">
        <div>
          <img className="card-img-top" src={sprites.front_default} alt={nama} />
          <InputSukses buttonLabel="catch pokemon" type={pokemon.types.map((poke) => poke.type.name).join(', ')} image={sprites.front_default}/>
        </div>
        <div className="card-body text-left">
          <h4 className="card-title text-center">{nama}</h4>
          <div className="col-12 mb-2">
            <div><b>Overview</b></div>
            <div className="row mb-2">
              <div className="col-7">
                <div>Height</div>
                <div>Weight</div>
                <div>Base Experience</div>
              </div>
              <div className="col-5">
                <div>: {pokemon.height}</div>
                <div>: {pokemon.weight}</div>
                <div>: {pokemon.base_experience}</div>
              </div>
            </div>
            <div><b>Type</b></div>
            <span className="mb-2">
              {pokemon.types.map((poke) => poke.type.name).join(', ')}
            </span>
            <div className="mt-2"><b>Stats</b></div>
            <div className="row mb-2">
              <div className="col-7">
                <div>HP</div>
                <div>Attack</div>
                <div>Defence</div>
                <div>Special Attack</div>
                <div>Special Defence</div>
                <div>Speed</div>
              </div>
              <div className="col-5">
                <div className="mb-2">
                { pokemon.stats.map((stat, index) => <div key={`stats-${pokemon.name}-${index}`}>: {stat.base_stat}</div>) }
                </div>
              </div>
            </div>
            <div className="mt-2"><b>Moves</b></div>
            <span className="mb-2">
              {pokemon.moves.map((poke) => poke.move.name).join(', ')}
            </span>
          </div>
        </div>
      </div>
    </main>
  )
}

export default Post
const API_URL = config.pokemon.url
export async function getServerSideProps(context) {
  const { query } = context
  
  const pokemon_name = query.nama

  const response = await fetch(`${API_URL}/${pokemon_name}`)
  const json = await response.json()
  return {
    props: {
      pokemon: json
    }
  }
}