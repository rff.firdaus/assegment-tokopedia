// index.js
 
import useFetchPokemon from '../useRequest'
import Pokemon from '../components/Pokemon'
import Button from '../components/button'
import Head from 'next/head'
import { Spinner } from 'reactstrap'


export default function IndexPage() {
  const { result, error } = useFetchPokemon()
 
  if (error) return <h1>Something went wrong!</h1>
  if (!result) return <div className="text-center mr-80">
  <div>
    <Spinner style={{ width: '3rem', height: '3rem' }} />
  </div>
</div>
 
  return (
    <main className='App'>
      <Head>
        <link rel="icon" href="/favico.ico" />
      </Head>
      <h1>My pokemons</h1>
      <div className="row mb-4">
        <Button />
      </div>
      <div>
        {result.results.map((pokemon) => (
          <Pokemon pokemon={pokemon} key={pokemon.name} />
        ))}
      </div>
    </main>
  )
}