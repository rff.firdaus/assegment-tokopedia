import { useState } from 'react'
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import Loading from './loading'

const Input = (props) =>{
    const [name, setName] = useState("")
    let type = props.type
    let image = props.image
    let data = ''
    const handleSubmit = (e) => {
        if (JSON.parse( localStorage.getItem("data")) === null) {
            data = []
        } else {
            data = JSON.parse( localStorage.getItem("data"))
        }
        e.preventDefault()
        data.push({
            name,
            type,
            image
        })
        localStorage.setItem("data", JSON.stringify(data))
        setModal(false)
        setName("")
    }
    const {
        buttonLabel,
    } = props;
    const [counter, setCounter] = useState(0);
    const [modal, setModal ] = useState(false);
    const [modalgagal, setModalgagal ] = useState(false);
    const [modalloading, setModalloading ] = useState(false);
    const toggle = () => {
        setModalloading(true)
        setTimeout(() => {
            setModalloading(false)
            if (Math.random() < 0.5) {
                setCounter(p => p + 1)
                setModal(true)
            } else {
                setModalgagal(true)
            }
        }, 5000)
    }
    const close = () => {
        setModal(false)
        setModalgagal(false)
    }

    return (
        <div>
            <Button color="danger" onClick={toggle}>{buttonLabel}</Button>
            <Modal isOpen={modal} toggle={close}>
                <ModalHeader >congrats, you caught it</ModalHeader>
                <ModalBody>
                    <form onSubmit={handleSubmit}>
                        <div className="text-left">My Pokemon Name</div>
                        <input type="text" className="form-control mb-2" onChange={(e) => setName(e.target.value)} value={name}/>
                        <input type="text" className="d-none" onChange={(e) => setType(e.target.value)} value={type}/>
                        <img className="d-none" onChange={(e) => setImage(e.target.value)} value={image}/>
                        <div className="text-center">
                            <button type="submit" className="btn btn-secondary mr-2">Simpan</button>
                            <Button color="danger" onClick={close}>Cancel</Button>
                        </div>
                    </form>
                </ModalBody>
            </Modal>
            <Modal isOpen={modalgagal} toggle={close}>
                <ModalHeader >Sorry, your pokemon was not caught</ModalHeader>
                <ModalBody>
                    <div className="text-center">
                        <Button color="danger" onClick={close}>Try Again</Button>
                    </div>
                </ModalBody>
            </Modal>
            <Modal isOpen={modalloading} toggle={close}>
                <Loading />
            </Modal>
        </div>
    )
}

export default Input