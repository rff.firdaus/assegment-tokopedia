// Pokemon.js
 
import React from 'react'
import useFetchPokemon from '../useRequest'
import Link from 'next/link'
import { Spinner } from 'reactstrap'

 
export default function Pokemon({ pokemon }) {
  const { name } = pokemon
  const { result, error } = useFetchPokemon(name)

  if (error) return <h1>Something went wrong!</h1>
  if (!result) return <div className="text-center mr-80">
  <div>
    <Spinner style={{ width: '3rem', height: '3rem' }} />
  </div>
</div>

  return (
    <Link href="/detail/[nama]" as={`/detail/${name}`}>
        <div className='Card'>
          <span className='Card--id'>#{result.id}</span>
          <img
              className='Card--image'
              src={result.sprites.front_default}
              alt={name}
          />
          <h1 className='Card--name'>{name}</h1>
          <span className='Card--details'>
              {result.types.map((poke) => poke.type.name).join(', ')}
          </span>
        </div>
    </Link>
  )
}