// index.js
 
import useFetchPokemon from '../useRequest'
import Button from '../components/button'
import Mypokemon from '../components/Mypokemon'
import { Spinner } from 'reactstrap'


export default function IndexPage() {
  const { result, error } = useFetchPokemon()
 
  if (error) return <h1>Something went wrong!</h1>
  if (!result) return <div className="text-center mr-80">
  <div>
    <Spinner style={{ width: '3rem', height: '3rem' }} />
  </div>
</div>
 
  return (
    <main className='App'>
      <h1>My pokemons</h1>
      <div className="row mb-4">
        <Button />
      </div>
      <div>
        <Mypokemon />
      </div>
    </main>
  )
}