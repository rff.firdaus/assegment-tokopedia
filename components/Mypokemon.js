// Pokemon.js
 
import React from 'react'
import useFetchPokemon from '../useRequest'
import { useState } from 'react'
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

 
export default function Mypokemon() {
    let list = <h5 className="mt-80">You don't have pokemon :(</h5>
    let getData = ''
    if (localStorage.getItem("data") === []) {
        getData = []
    } else {
        getData = localStorage.getItem("data")
    }
    const { result, error } = useFetchPokemon()
    const [modalgagal, setModalgagal ] = useState(false);
    const toggle = (data) => {
        localStorage.setItem('modal', JSON.stringify(data));
        setModalgagal(true)
    }
    const remove = () => {
        let removes = JSON.parse(localStorage.getItem('modal'));
        let dataFilter = getData.filter(function(val, index){
            return removes.name !== val.name;
        });

        localStorage.setItem('data', JSON.stringify(dataFilter));
        setModalgagal(false)
    }
    const close = () => {
        setModalgagal(false)
    }
    getData = JSON.parse(getData)
    if (error) return <h1>Something went wrong!</h1>
    if (!result) return <div className="text-center mr-80">
        <div>
        <Spinner style={{ width: '3rem', height: '3rem' }} />
        </div>
    </div>
    if (getData !== null && getData.length > 0) {
        list = getData.map((data) => {
            return (
                <div className='Card' key={data.name} item={data.name} onClick={() => toggle(data)}>
                    <img
                        className='Card--image'
                        src={data.image}
                        alt={data.name}
                        value={data.name}
                    />
                    <h1 className='Card--name'>{data.name}</h1>
                    <span className='Card--details'>
                        {data.type}
                    </span>
                </div>
            )
        })
    }
    return (
        <>
        {list}
        <Modal isOpen={modalgagal} toggle={close}>
            <ModalHeader >Are you sure you want to delete pokemon?</ModalHeader>
            <ModalBody>
                <div className="text-center">
                    <Button color="secondary" className="mr-2" onClick={remove}>Remove</Button>
                    <Button color="danger" onClick={close}>Cancel</Button>
                </div>
            </ModalBody>
        </Modal>
        </>
  )
}